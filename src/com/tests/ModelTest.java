package com.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dean.test.model.TestModel;
import com.dean.transferobjects.City;
import com.dean.transferobjects.CityImpl;
import com.dean.transferobjects.Country;
import com.dean.transferobjects.CountryImpl;

public class ModelTest {

	private TestModel testModel;
	private City city1 = new CityImpl(1, "Test City 1", 1, Calendar
			.getInstance().getTime());
	private City city2 = new CityImpl(2, "Test City 2", 2, Calendar
			.getInstance().getTime());
	private City city3 = new CityImpl(3, "Test City 3", 3, Calendar
			.getInstance().getTime());
	private Country country1 = new CountryImpl(1, "Test Country 1", Calendar
			.getInstance().getTime());
	private Country country2 = new CountryImpl(2, "Test Country 2", Calendar
			.getInstance().getTime());
	private Country country3 = new CountryImpl(3, "Test Country 3", Calendar
			.getInstance().getTime());

	@Before
	public void setup() {
		testModel = new TestModel();

		List<City> cities = new ArrayList<City>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);

		List<Country> countries = new ArrayList<Country>();
		countries.add(country1);
		countries.add(country2);
		countries.add(country3);

		testModel.setCities(cities);
		testModel.setCountries(countries);
	}

	@Test
	public void testGetCities() {
		List<City> cities = testModel.getCities();
		assertNotNull(cities);
		assertEquals(3, cities.size());
	}

	@Test
	public void testGetCountries() {
		List<Country> countries = testModel.getCountries();
		assertNotNull(countries);
		assertEquals(3, countries.size());
	}

	@Test
	public void testSetSelectedCity() {
		testModel.setSelectedCity((CityImpl) city2);
		City selectedCity = testModel.getSelectedCity();
		assertSame(city2, selectedCity);
	}

	@Test
	public void testSetSelectedCountry() {
		testModel.setSelectedCountry((CountryImpl) country3);
		Country selectedCountry = testModel.getSelectedCountry();
		assertSame(country3, selectedCountry);
	}

	@Test
	public void testInvalidSelectedCityWhenFiltered() {
		testModel.setSelectedCity((CityImpl) city1);
		testModel.setSelectedCountry((CountryImpl) country3);
		assertEquals(null, testModel.getSelectedCity());
	}

	@Test
	public void testValidSelectedCityWhenFiltered() {
		testModel.setSelectedCity((CityImpl) city2);
		testModel.setSelectedCountry((CountryImpl) country2);
		assertSame(city2, testModel.getSelectedCity());
	}
}
