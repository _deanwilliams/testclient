package com.dean.test;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.apache.log4j.Logger;

import com.dean.test.controller.TestController;
import com.dean.test.model.TestModel;
import com.dean.test.model.tables.TestTableModel;
import com.dean.test.view.AbstractView;
import com.dean.transferobjects.City;
import com.dean.transferobjects.Country;

public class TestClient extends AbstractView {

	private static Logger logger = Logger.getLogger(TestClient.class.getName());

	private static final int COLUMN_PACK_WIDTH = 2;

	private TestController controller = null;

	private JFrame frame;

	private JComboBox cityComboBox;
	private JTextField cityTextField;
	private JTable table;
	private TableRowSorter<TestTableModel> sorter;

	/**
	 * @wbp.nonvisual location=30,627
	 */
	private final TestTableModel testTableModel = new TestTableModel();

	private JComboBox countryComboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestClient window = new TestClient();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestClient() {
		initialize();
		initialiseModelAndController();
	}

	/**
	 * Initialise the model and controller
	 * 
	 * @return void
	 */
	private void initialiseModelAndController() {
		logger.info("Initialising Model and Controller");
		TestModel testModel = new TestModel();
		controller = new TestController();

		controller.addView(this);
		controller.addModel(testModel);

		try {
			testModel.initDefaults();
		} catch (NamingException e) {
			logger.error("Naming Exception", e);
			JOptionPane.showMessageDialog(frame, "Naming Exception",
					"Fatal Error", JOptionPane.ERROR_MESSAGE);
		} catch (EJBException e) {
			logger.error("EJB Exception", e);
			JOptionPane.showMessageDialog(frame, "EJB Exception",
					"Fatal Error", JOptionPane.ERROR_MESSAGE);
		} catch (FileNotFoundException e) {
			logger.error("File Not Found Exception", e);
			JOptionPane.showMessageDialog(frame, "File Not Found Exception",
					"Fatal Error", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			logger.error("IO Exception", e);
			JOptionPane.showMessageDialog(frame, "IO Exception", "Fatal Error",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Initialise the contents of the frame.
	 */
	private void initialize() {
		logger.info("Initialising GUI Components");
		frame = new JFrame();
		frame.setBounds(100, 100, 444, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		cityComboBox = new JComboBox();
		cityComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cityComboBox.getSelectedIndex() == 0) {
					setSelectedCity(null);
				} else {
					setSelectedCity((City) cityComboBox.getSelectedItem());
				}
			}
		});
		cityComboBox.setBounds(100, 7, 334, 24);
		panel.add(cityComboBox);

		JLabel lblCities = new JLabel("Cities : ");
		lblCities.setBounds(12, 12, 70, 15);
		panel.add(lblCities);

		JLabel lblSelected = new JLabel("Selected : ");
		lblSelected.setBounds(12, 49, 80, 15);
		panel.add(lblSelected);

		cityTextField = new JTextField();
		cityTextField.setEditable(false);
		cityTextField.setBounds(100, 47, 334, 19);
		panel.add(cityTextField);
		cityTextField.setColumns(10);

		table = new JTable();
		sorter = new TableRowSorter<TestTableModel>(testTableModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setAutoCreateColumnsFromModel(true);

		testTableModel.setColumnNames(new String[] { "City ID", "City Name",
				"Country ID", "Last Updated" });
		testTableModel.addTableModelListener(new TableModelListener() {

			@Override
			public void tableChanged(TableModelEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						packColumns(table, COLUMN_PACK_WIDTH);
					}
				});
			}

		});
		table.setModel(testTableModel);
		table.setRowSorter(sorter);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {

					@Override
					public void valueChanged(ListSelectionEvent lse) {
						if (!lse.getValueIsAdjusting()) {

							int viewRow = table.getSelectedRow();
							if (viewRow < 0) {
								// No Selected Row
							} else {
								int modelRow = table
										.convertRowIndexToModel(viewRow);
								setSelectedCity(getCityFromRow(modelRow));
							}
						}
					}

				});

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(12, 76, 422, 215);
		scrollPane.setViewportView(table);
		panel.add(scrollPane);

		JLabel lblFilterByCountry = new JLabel("Filter By Country : ");
		lblFilterByCountry.setBounds(12, 308, 145, 18);
		panel.add(lblFilterByCountry);

		countryComboBox = new JComboBox();
		countryComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (countryComboBox.getSelectedIndex() == 0) {
					// Clear the Selection
					setSelectedCountry(null);
				} else {
					setSelectedCountry((Country) countryComboBox
							.getSelectedItem());
				}
			}
		});
		countryComboBox.setBounds(169, 303, 265, 28);
		panel.add(countryComboBox);
	}

	/**
	 * Set the Selected City
	 * 
	 * @param city
	 */
	private void setSelectedCity(City city) {
		controller.changeSelectedCity(city);
	}

	/**
	 * Set the selected country
	 * 
	 * @param country
	 */
	private void setSelectedCountry(Country country) {
		if (country == null) {
			filterTable(null);
		} else {
			controller.changeSelectedCountry(country);
		}
	}

	/**
	 * Get city from table model
	 * 
	 * @param index
	 * @return
	 */
	private City getCityFromRow(int index) {
		return testTableModel.getAllItems().get(index);
	}

	/**
	 * Pack the Table Columns
	 * 
	 * @param table
	 * @param margin
	 */
	private void packColumns(JTable table, int margin) {
		DefaultTableColumnModel colModel = (DefaultTableColumnModel) table
				.getColumnModel();

		Enumeration<TableColumn> e = colModel.getColumns();
		int vColIndex = 0;
		while (e.hasMoreElements()) {
			TableColumn col = e.nextElement();
			int width = 0;

			// Get width of column header
			TableCellRenderer renderer = col.getHeaderRenderer();
			if (renderer == null) {
				renderer = table.getTableHeader().getDefaultRenderer();
			}
			Component comp = renderer.getTableCellRendererComponent(table,
					col.getHeaderValue(), false, false, 0, 0);
			width = comp.getPreferredSize().width;

			// Get maximum width of column data
			for (int r = 0; r < table.getRowCount(); r++) {
				renderer = table.getCellRenderer(r, vColIndex);
				comp = renderer.getTableCellRendererComponent(table,
						table.getValueAt(r, vColIndex), false, false, r,
						vColIndex);
				width = Math.max(width, comp.getPreferredSize().width);
			}

			// Add margin
			width += COLUMN_PACK_WIDTH * margin;

			// Set the width
			col.setPreferredWidth(width);

			vColIndex++;
		}
	}

	/**
	 * Receive and process any events from the model
	 * 
	 * @param PropertyChangeEvent
	 * @return void
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void modelPropertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(TestController.LOAD_CITIES)) {
			List<City> cities = (List<City>) evt.getNewValue();
			loadCities(cities);
		}
		if (evt.getPropertyName().equals(TestController.CITY_SELECTED)) {
			City newCity = (City) evt.getNewValue();
			citySelected(newCity);
		}
		if (evt.getPropertyName().equals(TestController.LOAD_COUNTRIES)) {
			List<Country> countries = (List<Country>) evt.getNewValue();
			loadCountries(countries);
		}
		if (evt.getPropertyName().equals(TestController.COUNTRY_SELECTED)) {
			Country newCountry = (Country) evt.getNewValue();
			countrySelected(newCountry);
		}
	}

	/**
	 * Load cities
	 * 
	 * @param cities
	 */
	private void loadCities(List<City> cities) {
		logger.info("Loading Cities");
		testTableModel.populateData(cities);
		cityComboBox.removeAllItems();
		cityComboBox.addItem("No City");
		for (City c : cities) {
			cityComboBox.addItem(c);
		}
		cityComboBox.repaint();
	}

	/**
	 * Load countries
	 * 
	 * @param countries
	 */
	private void loadCountries(List<Country> countries) {
		logger.info("Loading Countries");
		countryComboBox.removeAllItems();
		countryComboBox.addItem("No Country");
		for (Country c : countries) {
			countryComboBox.addItem(c);
		}
		countryComboBox.repaint();
	}

	/**
	 * City Selected
	 * 
	 * @return void
	 */
	private void citySelected(City city) {
		if (city != null) {
			logger.info("City Selected - " + city.getCityID() + " : "
					+ city.getCity());

			cityTextField.setText(city.getCity());
			setTableSelection(city);
			setComboBoxSelection(city);
		} else {
			logger.info("City de-selected");
			cityTextField.setText("");
			cityComboBox.setSelectedIndex(0);
		}
	}

	/**
	 * Country Selected
	 * 
	 * @param country
	 */
	private void countrySelected(Country country) {
		logger.info("Country Selected - " + country.getCountry());
		filterTable(country);
	}

	/**
	 * Filter the table based on supplied country
	 * 
	 * @param country
	 */
	private void filterTable(Country country) {
		RowFilter<TestTableModel, Object> rf = null;
		if (country != null) {
			// If current expression doesn't parse, don't update.
			try {
				rf = RowFilter.regexFilter("^" + country.getCountryID() + "$",
						table.getColumn("Country ID").getModelIndex());
			} catch (java.util.regex.PatternSyntaxException e) {
				return;
			}
		}
		sorter.setRowFilter(rf);
	}

	/**
	 * Set the combo box selection
	 * 
	 * @param city
	 */
	private void setComboBoxSelection(City city) {
		cityComboBox.setSelectedItem(city);
	}

	/**
	 * Set Table Selection
	 * 
	 * @param city
	 */
	private void setTableSelection(City city) {
		List<City> cities = testTableModel.getAllItems();
		int modelIndex = 0;
		for (City c : cities) {
			if (c.getCityID() == city.getCityID()) {
				break;
			}
			modelIndex++;
		}
		int viewIndex = table.convertRowIndexToView(modelIndex);
		table.changeSelection(viewIndex, 0, false, false);
	}
}
