package com.dean.test.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import com.dean.connectivity.DataBeanRemote;
import com.dean.test.controller.TestController;
import com.dean.transferobjects.City;
import com.dean.transferobjects.CityImpl;
import com.dean.transferobjects.Country;
import com.dean.transferobjects.CountryImpl;

public class TestModel extends AbstractModel {

	protected List<City> cities = null;
	protected City selectedCity = null;
	protected List<Country> countries = null;
	protected Country selectedCountry = null;

	/**
	 * Initialise the default values of this model
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NamingException
	 */
	public void initDefaults() throws FileNotFoundException, IOException,
			NamingException {
		setCities(loadCitiesFromDatabase());
		setCountries(loadCountriesFromDatabase());
	}

	/**
	 * Load the cities from the Database
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws NamingException
	 */
	private List<City> loadCitiesFromDatabase() throws FileNotFoundException,
			IOException, NamingException {
		DataBeanRemote bean = getDataBeanRemote();
		return bean.getAllCities();
	}

	private List<Country> loadCountriesFromDatabase()
			throws FileNotFoundException, IOException, NamingException {
		DataBeanRemote bean = getDataBeanRemote();
		return bean.getAllCountries();
	}

	/**
	 * Set the cities variable in this model
	 * 
	 * @param cities
	 */
	public void setCities(List<City> cities) {
		List<City> oldValue = this.cities;
		this.cities = cities;
		firePropertyChange(TestController.LOAD_CITIES, oldValue, cities);
	}

	/**
	 * Set the countries variable in this model
	 * 
	 * @param countries
	 */
	public void setCountries(List<Country> countries) {
		List<Country> oldValue = this.countries;
		this.countries = countries;
		firePropertyChange(TestController.LOAD_COUNTRIES, oldValue, countries);
	}

	/**
	 * Get the cities
	 * 
	 * @return
	 */
	public List<City> getCities() {
		if (cities == null) {
			setCities(new ArrayList<City>());
		}
		return cities;
	}

	/**
	 * Get the countries
	 * 
	 * @return
	 */
	public List<Country> getCountries() {
		if (countries == null) {
			setCountries(new ArrayList<Country>());
		}
		return countries;
	}

	/**
	 * Set the selected city
	 * 
	 * @param newCity
	 */
	public void setSelectedCity(CityImpl newCity) {
		City oldCity = selectedCity;
		selectedCity = newCity;
		if (oldCity == newCity)
			return;
		firePropertyChange(TestController.CITY_SELECTED, oldCity, newCity);
	}

	/**
	 * Return the currently selected city
	 * 
	 * @return
	 */
	public City getSelectedCity() {
		return selectedCity;
	}

	/**
	 * Set the selected country
	 * 
	 * @param newCountry
	 */
	public void setSelectedCountry(CountryImpl newCountry) {
		Country oldCountry = selectedCountry;
		selectedCountry = newCountry;
		if (selectedCity != null && selectedCity.getCountryID() != selectedCountry.getCountryID()) {
			// The selected country filters out the currently selected city
			setSelectedCity(null);
		}
		if (oldCountry == newCountry)
			return;
		firePropertyChange(TestController.COUNTRY_SELECTED, oldCountry,
				newCountry);
	}

	/**
	 * Get the currently selected country
	 * 
	 * @return
	 */
	public Country getSelectedCountry() {
		return selectedCountry;
	}
}
