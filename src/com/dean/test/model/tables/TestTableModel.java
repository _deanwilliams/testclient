package com.dean.test.model.tables;

import com.dean.transferobjects.City;

public class TestTableModel extends GenericTableModel<City> {

	private static final long serialVersionUID = 1L;

	@Override
	public Object getValueAt(int row, int column) {
		Object val = null;
		City city = data.get(row);
		if (city != null) {
			switch (column) {
			case 0:
				val = city.getCityID();
				break;
			case 1:
				val = city.getCity();
				break;
			case 2:
				val = city.getCountryID();
				break;
			case 3:
				val = city.getLastUpdate();
				break;
			}
		}
		return val;
	}
	
}
