package com.dean.test.model.tables;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * A Generic Table Model which accepts any List<T> for populating
 * the data. Now any of our table models can be declared with type <T>
 * and we do not need to implement a populate method and column type
 * methods for each and every one. The methods needing implementing
 * are the one to pull values from the stored data
 * 
 * @author Dean Williams
 *
 * @param <T>
 */
public abstract class GenericTableModel<T> extends AbstractTableModel {

	private static final long serialVersionUID = 1L;

	protected String[] columns = new String[] {
			"Column 1", "Column 2", "Column 3", "Column 4"
	};
	
	protected List<T> data = new ArrayList<T>();
	
	public void setColumnNames(String[] columns) {
		this.columns = columns;
	}
	
	@Override
	public String getColumnName(int column) {
		return columns[column];
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public abstract Object getValueAt(int row, int column);

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return getValueAt(0, columnIndex)==null?String.class:getValueAt(0, columnIndex).getClass();
	}
	
	/**
	 * Populate this table model with data
	 * 
	 * @param data
	 */
	public void populateData(List<T> data) {
		if (this.data == null) {
			this.data = new ArrayList<T>();
		}
		if (this.data.size() > 0) {
			this.data.clear();
		}
		if (data == null) {
			data = new ArrayList<T>();
		}
		this.data.addAll(data);
		fireTableDataChanged();
	}

	/**
	 * Get all the data
	 * 
	 * @return
	 */
	public List<T> getAllItems() {
		return data;
	}
}
