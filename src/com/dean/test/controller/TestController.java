package com.dean.test.controller;

import com.dean.transferobjects.City;
import com.dean.transferobjects.Country;

public class TestController extends AbstractController {

	public static final String LOAD_CITIES = "cities";
	public static final String LOAD_COUNTRIES = "countries";

	public static final String CITY_SELECTED = "SelectedCity";
	public static final String COUNTRY_SELECTED = "SelectedCountry";
	
	public void changeSelectedCity(City newCity) {
		setModelProperty(CITY_SELECTED, newCity);
	}
	
	public void changeSelectedCountry(Country newCountry) {
		setModelProperty(COUNTRY_SELECTED, newCountry);
	}
}
